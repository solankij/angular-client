import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from './auth.service';
import { Subscription, Observable } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  private subscriptions: Subscription[] = [];
  public loginForm: FormGroup;
  public isInvalid: boolean;
  public activateLoader$: Observable<boolean>;
  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router) { }

  ngOnInit() {
    this.createLoginForm();
    this.activateLoader$ = this.authService.isAuthenticating();
  }

  public createLoginForm() {
    this.loginForm = this.fb.group({
      username: this.fb.control('', [Validators.required]),
      password: this.fb.control('', [Validators.required])
    });
  }

  public submit() {
    this.subscriptions.push(
      this.authService.authenticate(this.loginForm.value)
        .subscribe((res: { success: string, message: string, token: string, user: { username: string } }) => {
          if (res.success && res.token) {
            this.authService.authenticated(res.token, res.user);
            this.router.navigate(['/home']);
            // display success message
            this.loginForm.reset();
          } else {
            this.isInvalid = true;
            this.loginForm.reset();
          }
        }));
  }
}
