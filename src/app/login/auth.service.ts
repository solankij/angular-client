import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { tap } from 'rxjs/operators';
import { CONFIG } from 'src/environments/config';


export interface AuthUser {
  username: String;
  email?: String;
  password: String;
}
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private domain: String = CONFIG.domain;
  // islooged in or not
  private isLoggedInSource = new BehaviorSubject<boolean>(false);
  public isLoggedIn$ = this.isLoggedInSource.asObservable();
  // token to validate request
  public authToken: string;
  public user: Object;

  // loader to indicate login status
  private isAuthenticatingSource = new BehaviorSubject<boolean>(false);
  public isAuthenticating$ = this.isAuthenticatingSource.asObservable();
  constructor(private http: HttpClient) {
    this.isLoggedIn() ? this.isLoggedInSource.next(true) : this.isLoggedInSource.next(false);
  }

  authenticate(user: AuthUser): Observable<any> {
    this.isAuthenticatingSource.next(true);
    return this.http.post(`${this.domain}/login`, user).pipe(tap(res => this.isAuthenticatingSource.next(false)));
  }

  isAuthenticating(): Observable<boolean> {
    return this.isAuthenticating$;
  }

  storeUserData(token, user) {
    localStorage.setItem('token', token);
    localStorage.setItem('user', JSON.stringify(user));
    this.authToken = token;
    this.user = user;
  }

  logout() {
    this.authToken = null;
    this.user = null;
    localStorage.clear();
    this.isLoggedInSource.next(false);
  }

  authenticated(token: string, user: any) {
    this.isLoggedInSource.next(true);
    this.storeUserData(token, user);
  }

  isLoggedIn() {
    this.authToken = localStorage.getItem('token');
    return (this.authToken == null) ? false : true;
  }

}
