import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavbarComponent } from './navbar/navbar.component';

import { RouterModule } from '@angular/router';
import { routes } from './app.routing';
import { ReactiveFormsModule, FormsModule } from '../../node_modules/@angular/forms';
import { SharedModule } from './shared/shared.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './shared/shared/auth-interceptor';
import { LoginComponent } from './login/login.component';
import { AuthService } from './login/auth.service';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ContentInterceptor } from './shared/shared/content-interceptor';
import { AuthGuardService } from './guards/auth-guard.service';



@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    DashboardComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes),
    SharedModule
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptor,
    multi: true
  },
    AuthService,
    AuthGuardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
