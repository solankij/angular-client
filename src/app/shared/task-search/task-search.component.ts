import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, distinct } from 'rxjs/operators';

@Component({
  selector: 'task-search',
  templateUrl: './task-search.component.html',
  styleUrls: ['./task-search.component.css'],

})
export class TaskSearchComponent implements OnInit {
  private subscriptions: Subscription[] = [];
  public searchForm: FormGroup;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.createForm()
  }

  ngOnDestroy() {
    this.subscriptions.map(sub => sub.unsubscribe())
  }

  createForm() {
    this.searchForm = this.fb.group({
      search: this.fb.control("")
    })
  }

}
