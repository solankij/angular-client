import { Pipe, PipeTransform } from "@angular/core";

function getVal(val, index) {
    return val[index];
  }
  @Pipe({
    name: "displayData"
  })
  export class DisplayDataPipe implements PipeTransform {
    transform(value, obj) {
      return getVal(value, obj['key']);
    }
  }