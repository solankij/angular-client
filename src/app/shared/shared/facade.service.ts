import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class FacadeService {
  private domain: String = `http://localhost:3200`;
  private authToken: String = '';


  constructor(private http: HttpClient) { }


/*   get(url: string, queryParams: any) {
    return this.http.get(this.domain queryParams);
  }

  update(url:string,payload:any){
    return this.http.put(this.domain)
  } */
}
