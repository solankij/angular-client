import {
  Component, OnInit, Input,
  Output, EventEmitter, ContentChild,
  OnDestroy, AfterContentInit, OnChanges
} from '@angular/core';
import { Observable, Subscription, of } from 'rxjs';
import { TaskSearchComponent } from '../task-search/task-search.component';
import { TaskPaginatorComponent } from '../task-paginator/task-paginator.component';
import { debounceTime } from 'rxjs/operators';


@Component({
  selector: 'task-table',
  templateUrl: './task-table.component.html',
  styleUrls: ['./task-table.component.css']
})

export class TaskTableComponent implements OnInit, OnDestroy, OnChanges, AfterContentInit {
  private subscriptions: Subscription[] = [];
  @Input() displayedColumns: Array<any>;
  @Input() dataSources: Observable<any>;
  @Input() headerConfig: Array<any>;
  @Output() actions: EventEmitter<any> = new EventEmitter();
  @Output() fetchData: EventEmitter<any> = new EventEmitter();
  @ContentChild(TaskSearchComponent, { static: false }) private searchTask: TaskSearchComponent;
  @ContentChild(TaskPaginatorComponent, { static: false }) public paginateTask: TaskPaginatorComponent;


  // query to send for table data
  public search = '';
  public pageIndex = 0;
  public pageSize = 5;
  constructor() { }

  ngOnInit() {

  }

  ngAfterContentInit() {
    this.subscriptions.push(this.searchTask.searchForm.valueChanges.pipe(debounceTime(300))
      .subscribe(res => {
        this.search = res.search;
        this.pageIndex = 0;
        this.fetchData.emit({ pageIndex: this.pageIndex, pageSize: this.pageSize, search: this.search });
      }
      ));
    this.subscriptions.push(this.paginateTask.page$.subscribe(res => {
      this.pageIndex = res.pageIndex;
      this.pageSize = res.pageSize;
      this.fetchData.emit({ pageIndex: this.pageIndex, pageSize: this.pageSize, search: this.search });

    }));
  }


  ngOnChanges() {
    setTimeout(() => {
      this.subscriptions.push(this.dataSources.subscribe((data: { total: Number, values: Array<any> }) => {
        this.paginateTask.total$.next(Number(data.total));
      }));
    }, 2);
  }
  ngOnDestroy() {
    this.subscriptions.map(sub => sub.unsubscribe());
  }

  public dispatchAction(data: any, action: any) {
    this.actions.emit({ data, action });
  }

  public trackByFn(index, obj) {
    return obj.$key;
  }
}
