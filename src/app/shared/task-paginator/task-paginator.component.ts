import { Component, OnInit, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { Subject, Observable, BehaviorSubject, Subscription } from 'rxjs';
import { PageEvent } from '@angular/material';

@Component({
  selector: 'task-paginator',
  templateUrl: './task-paginator.component.html',
  styleUrls: ['./task-paginator.component.css']
})
export class TaskPaginatorComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = []
  total = 0
  total$ = new BehaviorSubject(0)
  pageIndex = 0;
  pageSize = 5;
  page$ = new BehaviorSubject<{ pageIndex: number, pageSize: number }>({ pageIndex: 0, pageSize: 5 })
  constructor(private cdr: ChangeDetectorRef) { }

  ngOnInit() {
    this.subscriptions.push(this.total$.subscribe(total => {
      this.total = total
      this.cdr.markForCheck()
    }))
  }

  pageChanged(data) {
    this.page$.next({ pageIndex: parseInt(data.pageIndex), pageSize: parseInt(data.pageSize) })
  }

  ngOnDestroy() {
    this.subscriptions.map(sub => sub.unsubscribe())
  }

}
