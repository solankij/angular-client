import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskPaginatorComponent } from './task-paginator.component';

describe('TaskPaginatorComponent', () => {
  let component: TaskPaginatorComponent;
  let fixture: ComponentFixture<TaskPaginatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaskPaginatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskPaginatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
