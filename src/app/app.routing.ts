import { Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { AuthGuardService as AuthGuard } from './guards/auth-guard.service';

export const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'home', component: DashboardComponent },
  { path: 'contact', loadChildren: 'src/app/contact/contact.module#ContactModule', canActivate: [AuthGuard] },
  { path: 'task-reminder', loadChildren: 'src/app/task-reminder/task-reminder.module#TaskReminderModule', canActivate: [AuthGuard] }
];
