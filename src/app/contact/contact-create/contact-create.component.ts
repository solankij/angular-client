import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ContactService } from '../shared/contact.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material';

@Component({
  selector: 'contact-create',
  templateUrl: './contact-create.component.html',
  styleUrls: ['./contact-create.component.css']
})
export class ContactCreateComponent implements OnInit {
  private subscriptions: Subscription[] = [];
  public contactForm: FormGroup;
  constructor(
    private fb: FormBuilder,
    private service: ContactService,
    private router: Router,
    public snackbar: MatSnackBar) { }

  ngOnInit() {
    this.createContactForm();
  }

  public create() {
    this.subscriptions.push(this.service.createContact(this.contactForm.value).subscribe(res => {
      console.log(res,'res');
      this.openSnackBar('Contact has been created successfully!', 'CREATE');
      this.cancel();
    }));
  }

  public openSnackBar(message: string, action: string) {
    const config = new MatSnackBarConfig();
    config.panelClass = 'background-blue';
    config.duration = 2000;
    this.snackbar.open(message, action, config);
  }

  public createContactForm() {
    this.contactForm = this.fb.group({
      firstName: this.fb.control('', [Validators.required, Validators.minLength(2), Validators.maxLength(10)]),
      lastName: this.fb.control('', [Validators.required, Validators.minLength(2), Validators.maxLength(10)]),
      email: this.fb.control('', [Validators.required, Validators.email, Validators.minLength(5)]),
      phone: this.fb.control('', [Validators.required, Validators.minLength(6), Validators.maxLength(12)])
    });
  }

  public cancel() {
    this.router.navigate(['contact', 'list']);
  }
}
