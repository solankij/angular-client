import { Routes } from "@angular/router";
import { ContactComponent } from "./contact.component";
import { ContactListComponent } from "./contact-list/contact-list.component";
import { ContactCreateComponent } from "./contact-create/contact-create.component";
import { EditContactComponent } from "./edit-contact/edit-contact.component";
export const routes: Routes = [
    {
        path: '', component: ContactComponent, children: [
            { path: '', redirectTo: 'list', pathMatch: "full" },
            { path: 'list', component: ContactListComponent },
            { path: 'create', component: ContactCreateComponent },
            { path: 'edit', component: EditContactComponent }
        ]
    }
]