import { NgModule } from '@angular/core';
import { ContactComponent } from './contact.component';
import { SharedModule } from '../shared/shared.module';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule} from '@angular/common/http';
import { routes } from './contact.router';
import { ContactListComponent } from './contact-list/contact-list.component';
import { ContactService } from './shared/contact.service';
import { ContactCreateComponent } from './contact-create/contact-create.component';
import { EditContactComponent } from './edit-contact/edit-contact.component';
import { AuthInterceptor } from '../shared/shared/auth-interceptor';


@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    HttpClientModule

  ],
  exports: [],
  declarations: [ContactComponent, ContactListComponent, ContactCreateComponent, EditContactComponent],
  providers: [ContactService],
  entryComponents: [EditContactComponent],
  bootstrap: [ContactComponent]
})
export class ContactModule { }
