import { Component, OnInit, ChangeDetectorRef, AfterViewInit, AfterViewChecked } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog, MatSnackBar, MatSnackBarConfig } from '@angular/material';
import { ContactService, Page } from '../shared/contact.service';
import { tap, debounceTime } from 'rxjs/operators';
import { EditContactComponent } from '../edit-contact/edit-contact.component';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.css']
})
export class ContactListComponent implements AfterViewChecked {
  public dataSource$: Observable<any> = of({ total: 0, values: [] });
  public activateLoader = false;
  public displayedColumns = ['firstName', 'lastName', 'email', 'phone', 'edit', 'delete'];
  public headerConfig = [
    { key: 'firstName', label: 'First Name' },
    { key: 'lastName', label: 'Last Name' },
    { key: 'phone', label: 'Phone Number' },
    { key: 'email', label: 'Email' },
    { key: 'edit', label: 'Edit', action: 'create' },
    { key: 'delete', label: 'Delete', action: 'delete_sweep' }
  ];
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private cdr: ChangeDetectorRef,
    private contactService: ContactService,
    public snackbar: MatSnackBar,
    public dialog: MatDialog ) { }

  ngAfterViewChecked() {
    this.cdr.detectChanges();
  }

  public fetchData(data) {
    this.dataSource$ = this.getContacts(data);
  }

  public edit(task) {
    const dialogRef = this.dialog.open(EditContactComponent, {
      width: '950px',
      data: task
    });
    dialogRef.beforeClosed().toPromise().then(res => {
      this.fetchData({ pageIndex: 0, pageSize: 5, search: '' });
      this.cdr.markForCheck();
    });
  }

  public openSnackBar(message: string, action: string) {
    const config = new MatSnackBarConfig();
    config.panelClass = 'background-blue';
    config.duration = 2000;
    this.snackbar.open(message, action, config);
  }

  public delete(contact) {
    this.contactService.removeContact(contact).subscribe(res => {
      this.fetchData({ pageIndex: 0, pageSize: 5, search: '' });
      this.cdr.markForCheck();
      this.openSnackBar('Contact has been deleted succesfully!', 'Delete');
    });
  }

  public fetchActions(event) {
    if (event.action.key === 'edit') {
      this.edit(event.data);
    } else {
      this.delete(event.data);
    }
  }

  public create() {
    this.router.navigate(['contact', 'create']);
  }

  public trackByFn(index, obj) {
    return obj.$key;
  }

  private getContacts(query: Page) {
    this.activateLoader = true;
    return this.contactService.getContactByPage(query).pipe(tap((res) => {
      this.activateLoader = false;
    }));
  }
}
