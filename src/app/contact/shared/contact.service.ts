import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CONFIG } from 'src/environments/config';

export interface Page {
  pageIndex: number;
  pageSize: number;
  search: string;
}

export interface Contact {
  firstName: string;
  lastName: string;
  email: string;
  phone: number;
  id?: string;
}
@Injectable()
export class ContactService {
  private domain = CONFIG.domain;
  constructor(private http: HttpClient) { }

  getContactByPage(page: Page) {
    const queryParams = `?pageIndex=${page.pageIndex}&pageSize=${page.pageSize}&search=${page.search}`;
    return this.http.get(this.domain + '/tracker/contact/list' + queryParams);
  }

  createContact(contact: Contact) {
    return this.http.post(this.domain + '/tracker/contact/create', contact);
  }

  editContact(contact: Contact) {
    return this.http.put(this.domain + '/tracker/contact/edit/' + contact.id, contact);
  }

  removeContact(contact) {
    return this.http.delete(this.domain + '/tracker/contact/delete/' + contact._id);
  }
}
