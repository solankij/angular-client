import { Component, OnInit, Inject } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { ContactService } from '../shared/contact.service';
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBarConfig, MatSnackBar } from '@angular/material';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-edit-contact',
  templateUrl: './edit-contact.component.html',
  styleUrls: ['./edit-contact.component.css']
})
export class EditContactComponent implements OnInit {
  private subscriptions: Subscription[] = []
  public contactForm: FormGroup

  constructor(
    public dialogRef: MatDialogRef<EditContactComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private service: ContactService,
    public snackbar: MatSnackBar) { }

  ngOnInit() {
    this.createContactForm()
    if (this.data._id) {
      this.loadData(this.data)
    }
  }

  ngOnDestroy(){
    this.subscriptions.map(sub => sub.unsubscribe())
  }

  public createContactForm() {
    this.contactForm = this.fb.group({
      id: this.fb.control("", Validators.required),
      firstName: this.fb.control("", [Validators.required, Validators.minLength(2), Validators.maxLength(10)]),
      lastName: this.fb.control("", [Validators.required, Validators.minLength(2), Validators.maxLength(10)]),
      email: this.fb.control("", [Validators.required, Validators.email, Validators.minLength(5)]),
      phone: this.fb.control("", [Validators.required, Validators.minLength(6), Validators.maxLength(12)])
    })
  }

  public loadData(data) {
    this.contactForm.get("firstName").setValue(data.firstName)
    this.contactForm.get("lastName").setValue(data.lastName)
    this.contactForm.get("email").setValue(data.email)
    this.contactForm.get("phone").setValue(data.phone)
    this.contactForm.get("id").setValue(data._id)
  }


  public openSnackBar(message: string, action: string) {
    const config = new MatSnackBarConfig();
    config.panelClass = 'background-blue';
    config.duration = 2000
    this.snackbar.open(message, action, config)
  }

  public cancel(): void {
    this.dialogRef.close()
  }

  public save() {
    if (this.contactForm.value.id) {
     this.subscriptions.push( this.service.editContact(this.contactForm.value).subscribe(res => {
      this.openSnackBar("Contact has been edited successfully!", "EDIT")
      this.dialogRef.close()
    }))
    }

  }
}


