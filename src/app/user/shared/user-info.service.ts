import { Injectable } from '@angular/core'
import { Subject } from 'rxjs'
import { WebsocketService } from './websocket.sevice'

@Injectable({
  providedIn: 'root'
})
export class UserInfoService {
  users: Subject<any>
  addUser: Subject<any>
  removeUser: Subject<any>
  constructor(private wsService: WebsocketService) {
    /* 
    this.addUser = <Subject<any>>this.wsService.addUser()
    this.removeUser = <Subject<any>>this.wsService.removeUser() */
  }

  getUsers() {
    this.users = <Subject<any>>this.wsService.connect()
  }

  createUser(user) {
    this.wsService.addUser().next(user)
    /* this.addUser = <Subject<any>>this.wsService.addUser() */
}

  updateUser(userToUpdate) {
    this.wsService.UpdateUser().next(userToUpdate)
  }

  deleteUser(user) {
    this.wsService.removeUser().next(user)
  }
}
