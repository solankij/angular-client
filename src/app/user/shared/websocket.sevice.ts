import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { Observable } from 'rxjs';
import * as Rx from 'rxjs/';


@Injectable()
export class WebsocketService {

  // Our socket connection
  private socket;

  constructor() {

  }

  connect(): Rx.Subject<MessageEvent> {
    this.socket = io(`http://localhost:5000`)
    let observable = new Observable(observer => {
      this.socket.on('message', (data) => {

        observer.next(data);
      })
      return () => {
        this.socket.disconnect()
      }
    })
    let observer = {
      next: (data: Object) => {
        this.socket.emit('message', (data));
      },
    }
    return Rx.Subject.create(observer, observable);
  }

  addUser(): Rx.Subject<MessageEvent> {
    let observable2 = new Observable(observer => {
      this.socket.on('addMessage', (data) => {
        observer.next(data);
      })
      return () => {
        this.socket.disconnect();
      }
    })

    let observer = {
      next: (data: Object) => {
        this.socket.emit('addMessage', (data));
      },
    };
    return Rx.Subject.create(observer, observable2);

  }



  removeUser(): Rx.Subject<MessageEvent> {
    let observable3 = new Observable(observer => {
      this.socket.on('removeMessage', (data) => {
        observer.next(data);
      })
      return () => {
        this.socket.disconnect();
      }
    })

    let observer = {
      next: (data: Object) => {
        this.socket.emit('removeMessage', (data));
      },
    };
    return Rx.Subject.create(observer, observable3);
  }

  UpdateUser() {
    let observable4 = new Observable(observer => {
      this.socket.on('updateMessage', (data) => {
        observer.next(data);
      })
      return () => {
        this.socket.disconnect();
      }
    })

    let observer = {
      next: (data: Object) => {
        this.socket.emit('updateMessage', (data));
      },
    };
    return Rx.Subject.create(observer, observable4);
  }
}
