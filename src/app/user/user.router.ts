import { Routes } from "@angular/router";
import { UserComponent } from "./user.component";
import { UserListComponent } from "./user-list/user-list.component";



export const routes: Routes = [
    {
        path: '', component: UserComponent, children: [
            { path: '', redirectTo: 'list', pathMatch: 'full' },
            { path: 'list', component: UserListComponent },
           
        ]
    }
]