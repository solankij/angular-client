import { Component, OnInit } from '@angular/core';
import { UserInfoService } from './shared/user-info.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  constructor(private userInfoService: UserInfoService) {
    this.userInfoService.getUsers()
   }

  ngOnInit() {
  }

}
