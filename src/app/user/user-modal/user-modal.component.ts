import { Component, OnInit, Inject } from '@angular/core'
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { UserInfoService } from '../shared/user-info.service'


@Component({
  selector: 'app-user-modal',
  templateUrl: './user-modal.component.html',
  styleUrls: ['./user-modal.component.css']
})
export class UserModalComponent implements OnInit {
  public userForm: FormGroup
  constructor(
    public dialogRef: MatDialogRef<UserModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private userInfo: UserInfoService) { }

  ngOnInit() {
    this.createForm()
    if (this.data._id) {
      this.loadData(this.data)
    }
  }

  public createForm() {
    this.userForm = this.fb.group({
      id: this.fb.control(""),
      name: this.fb.control("", [Validators.required]),
      email: this.fb.control("", [Validators.required, Validators.email, Validators.minLength(5)]),
      phone: this.fb.control("", [Validators.required, Validators.minLength(10), Validators.maxLength(10)])
    })
  }

  public loadData(data) {
    this.userForm.get("name").setValue(data.name)
    this.userForm.get("email").setValue(data.email)
    this.userForm.get("phone").setValue(data.phone)
    this.userForm.get("id").setValue(data._id)
  }

  public cancel(): void {
    this.dialogRef.close()
  }

  public saveUser() {
    if (this.userForm.value.id) {
      this.userInfo.updateUser(this.userForm.value)
    } else {
      this.userInfo.createUser(this.userForm.value)
    }
    this.dialogRef.close()
  }

}
