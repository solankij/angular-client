import { NgModule } from "@angular/core"
import { UserComponent } from "./user.component"
import { RouterModule } from "@angular/router"
import { routes } from "./user.router"
import { UserListComponent } from './user-list/user-list.component'
import { UserInfoService } from "./shared/user-info.service"
import { WebsocketService } from "./shared/websocket.sevice"
import { UserModalComponent } from './user-modal/user-modal.component'
import { ReactiveFormsModule, FormsModule } from "../../../node_modules/@angular/forms"
import { SharedModule } from "../shared/shared.module"


@NgModule({
    imports: [
        RouterModule.forChild(routes),
        FormsModule,
        ReactiveFormsModule,
        SharedModule
    ],
    declarations: [UserComponent, UserListComponent, UserModalComponent],
    entryComponents: [UserModalComponent],
    providers: [UserInfoService, WebsocketService],
    bootstrap: [UserComponent]
})

export class UserModule { }