import { Component, OnInit, ViewChild } from '@angular/core'
import { UserInfoService } from '../shared/user-info.service'
import { Observable, Subject } from 'rxjs'
import { map } from 'rxjs/operators'
import { Router, ActivatedRoute } from '@angular/router'
import { MatDialog } from '@angular/material'
import { UserModalComponent } from '../user-modal/user-modal.component'
@Component({
  selector: 'user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
/** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['name', 'email', 'phone', 'edit', 'delete']
  dataSource$: Observable<any>

  constructor(private userInfoService: UserInfoService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    public dialog: MatDialog) {
  }

  ngOnInit() {

    this.dataSource$ = this.userInfoService.users.pipe(map(res => res.userList))
    /* .subscribe(response => console.log(response, "response from back"))*/
  }

  public edit(data) {
    this.openDialog({ name: data.name, email: data.email, phone: data.phone, _id: data._id })
  }

  public delete(user) {
    this.userInfoService.deleteUser(user)
  }

  public createUser() {
    this.openDialog()
    /*  this.router.navigate(["users","create"]) */
  }

  openDialog(userToEdit?): void {
    if (userToEdit) {
      const dialogRef = this.dialog.open(UserModalComponent, {
        width: '450px',
        data: { name: userToEdit.name, email: userToEdit.email, phone: userToEdit.phone, _id: userToEdit._id }
      })
    } else {
      const dialogRef = this.dialog.open(UserModalComponent, {
        width: '450px',
        data: { name: "", email: "", phone: "", _id: "" }
      })
    }
  }
}
