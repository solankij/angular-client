import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as moment from 'moment';
import { CONFIG } from 'src/environments/config';

export interface Page {
  pageIndex: number;
  pageSize: number;
  search: string;
}

export interface Task {
  date: any;
  time: string;
  note: string;
  status: string;
  contactId: string;
  _id?: string;
}

@Injectable()
export class TaskReminderService {
  private domain: string = CONFIG.domain;

  constructor(private http: HttpClient) { }

  createTask(task: Task) {
    return this.http.post(this.domain + '/tracker/task/create', task);
  }

  // Todo- create interface for page query to send
  getTaskByDate(dateTosend, page: Page) {
    const queryParams = `?date=${moment(new Date(dateTosend)).unix()}
    &pageIndex=${page.pageIndex}&pageSize=${page.pageSize}&search=${page.search}`;
    return this.http.get(this.domain + '/tracker/task/list' + queryParams);
  }

  editTask(task: Task) {
    return this.http.put(this.domain + '/tracker/task/edit/' + task._id, task);
  }

  deleteTask(id) {
    return this.http.delete(this.domain + '/tracker/task/delete/' + id);
  }

  getContacts(queryParams) {
    return this.http.get(this.domain + '/tracker/task/list' + queryParams);
  }

  getAllContacts() {
    return this.http.get(this.domain + '/tracker/contact/all');
  }
}

