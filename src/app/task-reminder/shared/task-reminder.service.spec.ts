import { TestBed, inject } from '@angular/core/testing';

import { TaskReminderService } from './task-reminder.service';

describe('TaskReminderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TaskReminderService]
    });
  });

  it('should be created', inject([TaskReminderService], (service: TaskReminderService) => {
    expect(service).toBeTruthy();
  }));
});
