import { Routes } from "@angular/router"
import { TaskReminderComponent } from "./task-reminder.component";
import { TaskListComponent } from "./task-list/task-list.component";
import { TaskCreationComponent } from "./task-creation/task-creation.component";
import { TaskWidgetComponent } from "./task-widget/task-widget.component";


export const routes: Routes = [
    {
        path: '', component: TaskReminderComponent,
        children: [
            { path: '', redirectTo: 'list', pathMatch: "full" },
            { path: 'list', component: TaskListComponent },
            { path: 'widget', component: TaskWidgetComponent },
            { path: 'create', component: TaskCreationComponent }
        ]
    }
]