import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '../../../node_modules/@angular/router';
import { routes } from './task-reminder.route';
import { TaskReminderComponent } from './task-reminder.component';
import { CommonModule } from '../../../node_modules/@angular/common';

import { FormsModule, ReactiveFormsModule } from '../../../node_modules/@angular/forms'
import { TaskListComponent } from './task-list/task-list.component';
import { TaskCreationComponent } from './task-creation/task-creation.component';
import { TaskReminderService } from './shared/task-reminder.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { EditTaskComponent } from './edit-task/edit-task.component';
import { TaskWidgetComponent } from './task-widget/task-widget.component';
import { WidgetViewComponent } from './task-widget/widget-view/widget-view.component';
import { TaskWidgetDirective } from './task-widget/task-widget.directive';
import { AuthInterceptor } from '../shared/shared/auth-interceptor';

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    HttpClientModule
  ],
  exports: [],
  declarations: [
    TaskReminderComponent,
    TaskListComponent,
    TaskCreationComponent,
    EditTaskComponent,
    TaskWidgetComponent,
    TaskWidgetDirective,
    WidgetViewComponent
  ],
  entryComponents: [EditTaskComponent, WidgetViewComponent],
  bootstrap: [TaskReminderComponent],
  providers: [TaskReminderService, {
    provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptor,
    multi: true
  }]
})

export class TaskReminderModule { }
