import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Router } from '@angular/router';
import { TaskReminderService, Page, Task } from '../shared/task-reminder.service';
import { MatDialog, MatSnackBar, MatSnackBarConfig } from '@angular/material';
import { EditTaskComponent } from '../edit-task/edit-task.component';
import { debounceTime, tap } from 'rxjs/operators';


@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TaskListComponent implements OnInit {
  public selectedTime: Date = new Date();
  public displayedColumns = ['time', 'status', 'note', 'edit', 'delete'];
  public dataSource$: Observable<any>;
  public activateLoader: boolean;
  public headerConfig = [
    { key: 'time', label: 'Time' },
    { key: 'status', label: 'Status' },
    { key: 'note', label: 'Note' },
    { key: 'edit', label: 'Edit', action: 'create' },
    { key: 'delete', label: 'Delete', action: 'delete_sweep' }
  ];

  constructor(
    private router: Router,
    private taskReminderService: TaskReminderService,
    public dialog: MatDialog,
    private cdr: ChangeDetectorRef,
    public snackbar: MatSnackBar) { }

  ngOnInit() { }


  public edit(task: Task) {
    const dialogRef = this.dialog.open(EditTaskComponent, {
      width: '950px',
      data: task
    });
    dialogRef.beforeClosed().toPromise().then(res => {
      this.getTaskByDate(this.selectedTime);
      this.cdr.markForCheck();
    });
  }

  public trackByFn(index, obj) {
    return obj.$key;
  }

  public fetchActions(event) {
    if (event.action.key === 'edit') {
      this.edit(event.data);
    } else {
      this.delete(event.data);
    }
  }

  public delete(task) {
    this.taskReminderService.deleteTask(task._id).subscribe(res => {
      this.getTaskByDate(this.selectedTime);
      this.cdr.markForCheck();
      this.openSnackBar('Task has been deleted succesfully!', 'Delete');
    });
  }

  public getTaskByDate(date) {
    this.activateLoader = true;
    this.dataSource$ = this.getTasks(date, { pageIndex: 0, pageSize: 5, search: '' }).pipe(tap(res => { this.activateLoader = false; }));
  }

  public openSnackBar(message: string, action: string) {
    const config = new MatSnackBarConfig();
    config.panelClass = 'background-blue';
    config.duration = 2000;
    this.snackbar.open(message, action, config);
  }

  public fetchData(query) {
    this.activateLoader = true;
    this.dataSource$ = this.getTasks(this.selectedTime, query).pipe(tap(res => { this.activateLoader = false; }));
  }

  public createTask() {
    this.router.navigate(['/task-reminder/create']);
  }
  public openWidgetView() {
    this.router.navigate(['/task-reminder/widget']);
  }

  private getTasks(date, query: Page) {
    return this.taskReminderService.getTaskByDate(date.toDateString(), query);/* .pipe(map((data:any) => data.values)) */
  }
}
