import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[task-widget]',
})
export class TaskWidgetDirective {
  constructor(public viewContainerRef: ViewContainerRef) { }
}