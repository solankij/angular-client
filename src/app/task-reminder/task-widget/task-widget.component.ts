import { Component, OnInit, ViewChild, ComponentFactoryResolver, ElementRef, TemplateRef, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { TaskWidgetDirective } from './task-widget.directive';
import { Page, TaskReminderService } from '../shared/task-reminder.service';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { WidgetViewComponent } from './widget-view/widget-view.component';
import flatpickr from 'flatpickr';
const moment = require('moment');
//const flatpickr = require('flatpickr');
@Component({
  selector: 'app-task-widget',
  templateUrl: './task-widget.component.html',
  styleUrls: ['./task-widget.component.css']
})
export class TaskWidgetComponent implements OnInit, AfterViewInit {
  @ViewChild(TaskWidgetDirective, { static: false }) public taskDir: TaskWidgetDirective;
  public activateLoader: boolean;
  public selectedTime: Date = new Date();
  public dataSource$: Observable<any>;
  @ViewChild('mydate', { static: true }) mydate: ElementRef;
  flat;
  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private taskReminderService: TaskReminderService,
    private cdr: ChangeDetectorRef) { }

  ngOnInit() {
    /*  this.dataSource$.subscribe(res => {
       console.log(res,"res")
       let componentFactory = this.componentFactoryResolver.resolveComponentFactory(WidgetViewComponent);
       let viewContainerRef = this.taskDir.viewContainerRef;
       viewContainerRef.clear()

       let componentRef = viewContainerRef.createComponent(componentFactory);
       (<WidgetViewComponent>componentRef.instance).info = {time:"2:pm",status:"active"}
     }) */
  }
  ngAfterViewInit() {
  this.flat =  flatpickr(this.mydate.nativeElement, {
      mode: "range",
      dateFormat: "Y-m-d",
      defaultDate: ["2019-07-17", "2019-07-20"],
      onChange:function(){
        self = this;

      }
  }
  );
    this.cdr.markForCheck();
  }

}
