import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TaskReminderService, Task } from '../shared/task-reminder.service';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import * as _ from 'lodash';
import { Subscription } from 'rxjs';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material';
import { map, tap } from 'rxjs/operators';
@Component({
  selector: 'app-task-creation',
  templateUrl: './task-creation.component.html',
  styleUrls: ['./task-creation.component.css']
})

export class TaskCreationComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];
  public taskForm: FormGroup;
  public hours = _.range(1, 13);
  public minutes = _.range(1, 61).map(num => ('0' + num).slice(-2));
  public contacts: any[];
  public status: any[] = ['To Do', 'Started', 'In Progress', 'Finished'];

  constructor(
    private fb: FormBuilder,
    private taskReminderService: TaskReminderService,
    private router: Router,
    public snackbar: MatSnackBar) { }

  ngOnInit() {
    this.createForm();
    this.taskForm.get('date').setValue(new Date());
    this.subscriptions.push(this.taskReminderService.getAllContacts()
      .pipe(map((ct: any) => ct.contacts))
      .subscribe(contacts => {
        console.log(contacts,"contacts");
        this.contacts = contacts;
      }));
  }

  ngOnDestroy() {
    this.subscriptions.map(sub => sub.unsubscribe());
  }

  public createForm() {
    this.taskForm = this.fb.group({
      hour: this.fb.control('', [Validators.required]),
      minute: this.fb.control('00'),
      date: this.fb.control('', [Validators.required]),
      note: this.fb.control('', [Validators.required]),
      timeType: this.fb.control('', [Validators.required]),
      contactId: this.fb.control('', [Validators.required]),
      status: this.fb.control('', [Validators.required])
    });
  }

  public save() {
    const selectedDate = moment(this.taskForm.get('date').value).format('YYYY-MM-DD');
    const selectedTime = this.taskForm.get('hour').value +
      ':' + this.taskForm.get('minute').value + ' ' + this.taskForm.get('timeType').value;
    const dateToSend = moment(selectedDate + ' ' + moment(selectedTime, 'h:mm A').format('HH:mm'));

    const task: Task = {
      date: dateToSend,
      time: selectedTime,
      note: this.taskForm.get('note').value,
      status: this.taskForm.get('status').value,
      contactId: this.taskForm.get('contactId').value
    };
    this.subscriptions.push(
      this.taskReminderService.createTask(task).subscribe((res: any) => {
        if (res.success == true) {
          this.openSnackBar('Task has been created successfully!', 'CREATE');
          this.router.navigate(['/task-reminder']);
        }
      }));
  }

  public openSnackBar(message: string, action: string) {
    const config = new MatSnackBarConfig();
    config.panelClass = 'background-blue';
    config.duration = 2000;
    this.snackbar.open(message, action, config);
  }

  public cancel() {
    this.router.navigate(['task-reminder']);
  }

  public reset() {
    this.taskForm.reset();
    this.taskForm.markAsUntouched();
  }
}
