import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar, MatSnackBarConfig } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as _ from 'underscore';
import { TaskReminderService, Task } from '../shared/task-reminder.service';
import * as moment from 'moment';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
@Component({
  selector: 'app-edit-task',
  templateUrl: './edit-task.component.html',
  styleUrls: ['./edit-task.component.css']
})
export class EditTaskComponent implements OnInit {
  private subscriptions: Subscription[] = [];
  public taskForm: FormGroup;
  public hours = _.range(1, 13);
  public minutes = _.range(1, 61).map(num => ('0' + num).slice(-2));
  public contacts: any[];
  public status: any[] = ['To Do', 'Started', 'In Progress', 'Finished'];

  constructor(public dialogRef: MatDialogRef<EditTaskComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private taskReminderService: TaskReminderService,
    public snackbar: MatSnackBar) { }

  ngOnInit() {
    this.createForm();
    this.loadForm(this.data);
    this.subscriptions.push(this.taskReminderService.getAllContacts()
      .pipe(map((ct: any) => ct.contacts))
      .subscribe(contacts => {
        this.contacts = contacts;
      }));
  }

  public loadForm(data) {
    const timeToSelect = data.time.split(':');
    const hour = parseInt(timeToSelect[0]);
    const minute = (timeToSelect[1].split(' ')[0]);
    const timeType = timeToSelect[1].split(' ')[1];
    this.taskForm.get('hour').setValue(hour);
    this.taskForm.get('minute').setValue(minute);
    this.taskForm.get('timeType').setValue(timeType);
    this.taskForm.get('note').setValue(data.note);
    this.taskForm.get('contactId').setValue(data.contactId);
    this.taskForm.get('status').setValue(data.status);
  }

  public createForm() {
    this.taskForm = this.fb.group({
      hour: this.fb.control('', [Validators.required]),
      minute: this.fb.control('00'),
      note: this.fb.control('', [Validators.required]),
      timeType: this.fb.control('', [Validators.required]),
      status: this.fb.control('', [Validators.required]),
      contactId: this.fb.control('', [Validators.required])
    });
  }

  public save() {
    const selectedDate = moment(this.data.date).format('YYYY-MM-DD');
    const selectedTime = this.taskForm.get('hour').value + ':' + this.taskForm.get('minute').value + ' ' + this.taskForm.get('timeType').value;
    const dateToSend = moment(selectedDate + ' ' + moment(selectedTime, 'h:mm A').format('HH:mm'));
    const task: Task = {
      _id: this.data._id,
      date: dateToSend,
      time: selectedTime,
      note: this.taskForm.get('note').value,
      status: this.taskForm.get('status').value,
      contactId: this.taskForm.get('contactId').value
    };
    this.taskReminderService.editTask(task).toPromise().then((res) => {
      this.dialogRef.close();
      this.openSnackBar('Task has been edited successfully!', 'Edit');
    });
  }

  public openSnackBar(message: string, action: string) {
    const config = new MatSnackBarConfig();
    config.panelClass = 'background-blue';
    config.duration = 2000;
    this.snackbar.open(message, action, config);
  }

  public cancel() {
    this.dialogRef.close();
  }
}
